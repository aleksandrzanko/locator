/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track } from 'lwc';
import LOGO from './../../icons/fleetcor-logo.png';
import './header.scss';

const IMAGES = {
    logo: LOGO
};

export default class Header extends LightningElement {
    images = IMAGES;

    @track _showLogout;

    @api set showLogout(value) {
        this._showLogout = value;
    }
    get showLogout() {
        return this._showLogout;
    }
}
