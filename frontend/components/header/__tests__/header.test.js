import { createElement } from 'lwc';
import Header from '../header';

function flushPromises() {
    // eslint-disable-next-line no-undef
    return new Promise(resolve => setImmediate(resolve));
}

describe('Frontend Component Header', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('header worked with logout button', () => {
        const element = createElement('z-header', {
            is: Header
        });
        document.body.appendChild(element);
        element.showLogout = true;
        let isLogoutVisible = element.showLogout;
        expect(isLogoutVisible).toBeTruthy();

        const buttonLogout = element.shadowRoot.querySelector('.menu-logo');
        expect(buttonLogout).not.toBeNull();

        return flushPromises().then(() => {
            let buttonLogoutEl = element.shadowRoot.querySelector(
                '.button-logout'
            );
            expect(buttonLogoutEl).not.toBeNull();

            element.showLogout = null;
            isLogoutVisible = element.showLogout;
            expect(isLogoutVisible).toBeFalsy();

            return flushPromises().then(() => {
                buttonLogoutEl = element.shadowRoot.querySelector(
                    '.button-logout'
                );
                expect(buttonLogoutEl).toBeNull();
            });
        });
    });
});
