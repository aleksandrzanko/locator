import { createElement } from 'lwc';
import Backups from '../backups';

function flushPromises() {
    // eslint-disable-next-line no-undef
    return new Promise(resolve => setImmediate(resolve));
}

describe('Frontend Component Backups', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('Backups with no backupsItems', () => {
        const element = createElement('z-backups', {
            is: Backups
        });
        document.body.appendChild(element);

        let backupsButtonElement = element.shadowRoot.querySelector(
            '.backups__button'
        );
        expect(backupsButtonElement).not.toBeNull();

        let startButtonElement = element.shadowRoot.querySelector(
            '.backups__start'
        );
        expect(startButtonElement).toBeNull();

        backupsButtonElement.click();
        backupsButtonElement = element.shadowRoot.querySelector(
            '.button_success'
        );
        expect(backupsButtonElement).not.toBeNull();

        return flushPromises().then(() => {
            startButtonElement = element.shadowRoot.querySelector(
                '.backups__start'
            );
            expect(startButtonElement).not.toBeNull();

            let emptyLiElement = element.shadowRoot.querySelector(
                '.backups__item_empty'
            );
            expect(emptyLiElement).not.toBeNull();
            expect(emptyLiElement.textContent).toBe('Empty List');

            backupsButtonElement.click();
            backupsButtonElement = element.shadowRoot.querySelector(
                '.button_success'
            );
            expect(backupsButtonElement).toBeNull();
        });
    });

    it('Backups with backupsItems', () => {
        const element = createElement('z-backups', {
            is: Backups
        });
        document.body.appendChild(element);

        element.backupsItems = [
            {
                label: 'Label 1',
                value: 'Value 1'
            },
            {
                label: 'Label 2',
                value: 'Value 2'
            },
            {
                label: 'Label 3',
                value: 'Value 3'
            }
        ];

        let backupsButtonElement = element.shadowRoot.querySelector(
            '.backups__button'
        );
        expect(backupsButtonElement).not.toBeNull();

        backupsButtonElement.click();

        return flushPromises().then(() => {
            let liElements = element.shadowRoot.querySelectorAll(
                '.backups__item'
            );
            expect(liElements.length).toBe(3);

            liElements[0].click();

            return flushPromises().then(() => {
                liElements = element.shadowRoot.querySelectorAll(
                    '.backups__item'
                );
                expect(liElements).not.toBeNull();
                liElements[1].click();
                backupsButtonElement = element.shadowRoot.querySelector(
                    '.backups__button'
                );
                backupsButtonElement.click();
                backupsButtonElement.click();

                return flushPromises().then(() => {
                    liElements = element.shadowRoot.querySelectorAll(
                        '.backups__item'
                    );
                    expect(liElements).not.toBeNull();

                    let startOpenElement = element.shadowRoot.querySelector(
                        '.backups__start'
                    );
                    expect(startOpenElement).not.toBeNull();
                    startOpenElement.click();
                    
                    liElements[1].click();
                    startOpenElement.click();
                });
            });
        });
    });
});
