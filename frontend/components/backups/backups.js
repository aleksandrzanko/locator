import { LightningElement, track, api } from 'lwc';
import './backups.scss';

export default class Backups extends LightningElement {
    @api backupsItems = [];

    @track isButtonOpen;

    _backupItem;

    toggleButton(event) {
        event.currentTarget.classList.toggle('button_success');
        this.isButtonOpen = !this.isButtonOpen;
        if (!this.isButtonOpen) {
            this._backupItem = null;
        }
    }

    selectBackupItem(event) {
        let items = this.template.querySelectorAll('.backups__item');
        items.forEach((element, inx) => {
            element.classList.remove('backups__item_current');
            if (element === event.currentTarget) {
                event.currentTarget.classList.add('backups__item_current');
                this._backupItem = this.backupsItems[inx];
                let buttonStart = this.template.querySelector(
                    '.backups__start'
                );
                buttonStart.classList.add(
                    'button_success',
                    'backups__start_open'
                );
            }
        });
    }

    async startRestore() {
        if (this._backupItem) {
            this.dispatchEvent(
                new CustomEvent('restore', {
                    detail: this._backupItem.value,
                    bubbles: true
                })
            );
        }
    }
}
