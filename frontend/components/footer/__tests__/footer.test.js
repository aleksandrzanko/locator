import { createElement } from 'lwc';
import Footer from '../footer';

describe('Frontend Component Footer', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('footer with copyright text', () => {
        const element = createElement('z-footer', {
            is: Footer
        });
        document.body.appendChild(element);

        const textElement = element.shadowRoot.querySelector('.copy');
        expect(textElement).not.toBeNull();
    });
});
