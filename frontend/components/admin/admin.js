/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable no-await-in-loop */
import { LightningElement, track } from 'lwc';
import './admin.scss';

export default class Admin extends LightningElement {
    @track backupVersions;
    @track batchId;
    @track totalResult;
    @track spinnerText = 'loading...';
    @track isLoginUser;
    @track isLoading = true;
    @track showResult = false;

    spinnerEnd() {
        this.isLoading = false;
    }

    spinnerStart() {
        this.isLoading = true;
    }

    async connectedCallback() {
        let response = await fetch('/admin/is-login');
        let result = await response.json();
        if (result.status) {
            this.loadVersions();
        } else {
            this.isLoading = false;
        }
    }

    async loadVersions() {
        let versionsResponse = await fetch('/admin/versions');
        let result = await versionsResponse.json();
        if (result.validate) {
            this.backupVersions = result.versions;
            this.isLoading = false;
            this.isLoginUser = true;
        } else {
            this.reloadPage();
        }
    }

    async checkStatus() {
        this.spinnerText = `updating locator database...`;
        let response = await fetch('/admin/check-status');
        let result = await response.json();
        if (result.oracleUpdating) {
            setTimeout(() => {
                this.checkStatus();
            }, 1000);
        } else {
            this.loadVersions();
        }
    }

    async startRestore(event) {
        const version = event.detail;
        this.template.querySelector('z-content').previewBackUp(version);
    }

    async updateDataBase(event) {
        this.isLoading = true;
        this.spinnerText = 'uploading 0%';
        const restoreId = event.detail.restoreId;
        let stations = event.detail.stations;

        const size = 1000;
        let index = 0;
        const total = stations.length;
        sendDataPartOfStations.bind(this)();

        function sendDataPartOfStations() {
            let stationsPart;
            let isLast;
            if (index < total) {
                stationsPart = stations.slice(index, (index += size));
            } else {
                stationsPart = stations.slice(index, stations.length);
                isLast = true;
            }

            fetch(`/admin/update-data-base`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    stations: stationsPart,
                    restoreId: restoreId,
                    isLast: isLast
                })
            })
                .then(response => response.json())
                .then(result => {
                    if (!result.sessionValid) {
                        this.reloadPage();
                    } else {
                        let percent = parseInt(
                            (result.uploaded * 100) / total,
                            10
                        );
                        this.spinnerText = `uploading ${percent}%`;
                        if (!result.status) {
                            sendDataPartOfStations.bind(this)();
                        } else {
                            this.checkStatus();
                        }
                    }
                });
        }
    }

    loginSuccessHandler() {
        this.isLoginUser = true;
        this.loadVersions();
    }

    userLogoutHandler() {
        this.isLoginUser = false;
    }

    async updateSalesforce() {
        this.isLoading = true;
        this.spinnerText = 'Salesforce accounts updating...';
        let response = await fetch(`/admin/update-salesforce`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({})
        });
        let result = await response.json();
        console.log(result);
        if (result.status) {
            this.checkSalesforceState(result.batchId);
        } else {
            alert(result.message);
            this.isLoading = false;
        }
    }

    async checkSalesforceState(batchId, countDone, countTotal, countErrors) {
        if (countDone || countTotal || countErrors) {
            this.spinnerText = `Updating process: 
                                total: ${countTotal} | 
                                complete: ${countDone}`;
        }

        let response = await fetch(`/admin/check-state-salesforce-updating`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({ batchId: batchId })
        });
        let result = await response.json();

        if (result.status) {
            if (result.isDone) {
                this.showResult = true;
                this.batchId = result.batchId;
                this.totalResult = result.countTotal;
                this.isLoading = false;
            } else {
                setTimeout(() => {
                    this.checkSalesforceState(
                        result.batchId,
                        result.countDone,
                        result.countTotal,
                        result.countErrors
                    );
                }, 2000);
            }
        } else {
            alert('Salesforce updating process error');
            this.isLoading = false;
        }
    }

    reloadPage() {
        document.location.reload(true);
    }

    closeSalesforceResult() {
        this.showResult = false;
    }
}
