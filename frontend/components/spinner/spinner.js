import { LightningElement, api } from 'lwc';
import './spinner.scss';
import IMG from './../../icons/spinner.svg';

export default class Spinner extends LightningElement {
    img = IMG;
    @api text;
}
