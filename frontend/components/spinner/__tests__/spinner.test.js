import { createElement } from 'lwc';
import Spinner from '../spinner';

describe('Frontend Component Spinner', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('spinner without text', () => {
        const element = createElement('z-spinner', {
            is: Spinner
        });
        document.body.appendChild(element);

        const textElement = element.shadowRoot.querySelector('.spinner__text');
        const imgElement = element.shadowRoot.querySelector('.spinner__img');
        expect(imgElement).not.toBeNull();
        expect(textElement).toBeNull();
    });

    it('spinner with text', () => {
        const element = createElement('z-spinner', {
            is: Spinner
        });
        element.text = 'Hello test';
        document.body.appendChild(element);

        const imgElement = element.shadowRoot.querySelector('.spinner__img');
        expect(imgElement).not.toBeNull();
        const textElement = element.shadowRoot.querySelector('.spinner__text');
        expect(textElement).not.toBeNull();
        expect(textElement.textContent).toBe('Hello test');
    });
});
