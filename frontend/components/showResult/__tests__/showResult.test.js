import { createElement } from 'lwc';
import ShowResult from '../showResult';

function flushPromises() {
    // eslint-disable-next-line no-undef
    return new Promise(resolve => setImmediate(resolve));
}

describe('Frontend Component ShowResult', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }

        global.fetch.mockClear();
        delete global.fetch;
    });

    it('result without errors', () => {
        const mockSuccessResponse = { url: 'https://example.com', report: {} };
        const mockJsonPromise = Promise.resolve(mockSuccessResponse); // 2
        const mockFetchPromise = Promise.resolve({
            // 3
            json: () => mockJsonPromise
        });
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

        const element = createElement('z-show-result', {
            is: ShowResult
        });

        element.total = 100;
        element.batchId = 'testid';

        document.body.appendChild(element);
        let spinnerEl = element.shadowRoot.querySelector('z-spinner');
        expect(spinnerEl).not.toBeNull();

        return flushPromises().then(() => {
            spinnerEl = element.shadowRoot.querySelector('z-spinner');
            expect(spinnerEl).toBeNull();
            let closeButtonEl = element.shadowRoot.querySelector('.button');
            closeButtonEl.click();
        });
    });

    it('result with errors', () => {
        const mockSuccessResponse = {
            url: 'https://example.com',
            report: {
                columns: [
                    {
                        id: 0,
                        value: 'Error title'
                    }
                ],
                errors: [
                    {
                        id: 1,
                        value: [
                            {
                                id: 0,
                                value: 'Error 1'
                            }
                        ]
                    },
                    {
                        id: 2,
                        value: [
                            {
                                id: 0,
                                value: 'Error 2'
                            }
                        ]
                    }
                ]
            }
        };
        const mockJsonPromise = Promise.resolve(mockSuccessResponse); // 2
        const mockFetchPromise = Promise.resolve({
            // 3
            json: () => mockJsonPromise
        });
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

        const element = createElement('z-show-result', {
            is: ShowResult
        });

        element.total = 100;
        element.batchId = 'testid';

        document.body.appendChild(element);
        let spinnerEl = element.shadowRoot.querySelector('z-spinner');
        expect(spinnerEl).not.toBeNull();

        return flushPromises().then(() => {
            spinnerEl = element.shadowRoot.querySelector('z-spinner');
            expect(spinnerEl).toBeNull();
            let errorsTableEl = element.shadowRoot.querySelector(
                '.show-result__table'
            );
            expect(errorsTableEl).not.toBeNull();
        });
    });
});
