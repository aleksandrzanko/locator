import { LightningElement, track, api } from 'lwc';
import './showResult.scss';

export default class ShowResult extends LightningElement {
    @api batchId;
    @api total;

    @track isLoading = true;
    @track actualColumns;
    @track actualData;
    @track success;
    @track urlFile;

    async connectedCallback() {
        let response = await fetch('/admin/get-report', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({
                batchId: this.batchId
            })
        });
        let result = await response.json();
        this.urlFile = result.url;
        this.actualColumns = result.report.columns;
        this.actualData = result.report.errors ? result.report.errors : [];
        this.success = this.total - this.actualData.length;
        this.isLoading = false;
    }

    closeReport() {
        this.dispatchEvent(new CustomEvent('closeresult'));
    }

    disconnectedCallback() {
        this.isLoading = true;
    }
}
