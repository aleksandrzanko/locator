const createError = require('http-errors'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    path = require('path'),
    session = require('express-session'),
    logger = require('morgan'),
    indexRouter = require('./backend/routes/index'),
    // adminRouter = require('./backend/routes/admin'),
    app = express(),
    bodyParser = require('body-parser'),
    cors = require('cors');

// view engine setup
app.use(bodyParser.json({ limit: '100mb' }));
app.use(cors());
app.set('views', path.join(__dirname, 'backend/views'));
app.set('view engine', 'pug');

app.use(logger('dev'));

app.set('trust proxy', 1);
app.use(cookieParser());
app.use(
    session({
        secret: 'enway',
        saveUninitialized: false,
        resave: true,
        rolling: true,
        cookie: { maxAge: 3600 * 1000 }
    })
);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
// app.use('/admin', adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
