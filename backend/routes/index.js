/* eslint-disable no-undef */
const express = require('express');
const router = express.Router();
const assets = require('../modules/assets/assets');
const session = require('../modules/session/session');
const CSVDataBase = require('../modules/CSVDataBase');
const LocatorOracleDB = require('../modules/LocatorOracleDB');
const VersionsDataBase = require('../modules/VersionsDataBase');
const batch = require('../modules/Salesforce/batch');
const multer = require('multer');
const upload = multer({ dest: 'backend/database/' });
const request = require('request');
const dns = require('dns'); 
const validator = require('email-validator');


const USER_NAME = 'admin';
const USER_PASSWORD = '8SOUYEP2W3VJ';
const SECRET_KEY = 'P!v^h9UOC2j&';
const PROXY = 'http://srv-proxy-prod.transitcard.ru:3128';
const SALESFORCE_URL =
    'https://int-e2efleetcor.cs107.force.com/services/apexrest/locator';
const SALESFORCE_CHECK_STATE_URL =
    'https://int-e2efleetcor.cs107.force.com/services/apexrest/locator-check-state';

/**
 * @description Welcome Page
 */
router.get('/', function(req, res, next) {
    delete session[req.session.id];
    delete req.session.stations;
    res.render('admin', {
        title: 'Admin panel',
        css: assets.admin.css,
        js: assets.admin.js
    });
});


const verifier = require('email-exist');

/**
 * @description Email exist
 */
router.get('/api/email-exist', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    const email = req.query.email;
	let result = {success:false, email:email}; 
	if(!email){
		result.message = 'email address is empty';
		res.send(result);
	}else{
        if(!validator.validate(email)){
            result.message = 'email address is no valid format';
		    res.send(result);
        }else{
            dns.resolveMx(email.split('@')[1], (err, addresses) => {
                if(addresses && addresses.length){
                    result.message = 'email address can be exist';
                    result.success = true;
                }else{
                    result.message = 'email address not exist';
                }
                res.send(result);
            }); 
        }
    }
});

/**
 * @description User is login
 */
router.get('/admin/is-login', function(req, res) {
    res.send({
        status: !!req.session.validate
    });
});

/**
 * @description login user
 */
router.post('/admin/login', async function(req, res) {
    const username = req.body.username,
        password = req.body.password,
        response = {
            status: false,
            message: `Error! Invalid username or password`
        };

    if (USER_NAME === username && USER_PASSWORD === password) {
        req.session.validate = true;
        response.status = true;
    } else {
        req.session.validate = false;
    }

    res.send(response);
});

/**
 * @description User logout
 */
router.get('/admin/logout', function(req, res) {
    delete session[req.session.id];
    delete req.session.stations;
    req.session.destroy();
    res.redirect('/');
});

/**
 * @description login user
 */
router.get('/admin/versions', async function(req, res) {
    let response = {};
    if (req.session.validate) {
        const versionsDB = VersionsDataBase();
        response.versions = versionsDB.getListOfVersions();
        response.validate = req.session.validate;
    }

    res.send(response);
});

/**
 * @description stations
 */
router.get('/admin/stations', async function(req, res) {
    let response = { version: null, stations: null, columns: null };
    if (req.session.validate) {
        response.validate = req.session.validate;
        const versionsDB = VersionsDataBase();
        let version = versionsDB.getCurrentVersion();
        response.version = version;
        if (response.version) {
            const csvDataBase = CSVDataBase();
            let allData = await csvDataBase.getDataByVersion(version.value);
            if (allData.length) {
                let header = allData.shift();
                response.columns = header.map((item, index) => {
                    return {
                        id: index,
                        value: item
                    };
                });
                response.stations = allData.map((station, indexStation) => {
                    return {
                        id: indexStation + 1,
                        value: station.map((item, index) => {
                            return {
                                id: index,
                                value: item
                            };
                        })
                    };
                });
            }
        }
    }
    res.send(response);
});

/**
 * @description Upload Xlsx
 */
router.post(
    '/admin/upload-xlsx',
    upload.single('xlsxFile'),
    async (req, res) => {
        let response = {
            stations: 0,
            columns: null,
            validate: false
        };
        if (req.session.validate) {
            response.sessionValid = req.session.validate;

            if (req.file.path) {
                const csvDataBase = CSVDataBase();
                let allData = await csvDataBase.previewData(req.file.path);
                if (allData.length) {
                    let header = allData.shift();
                    response.validate = await csvDataBase.isFileValid(header);
                    response.columns = header.map((item, index) => {
                        return {
                            id: index,
                            value: item
                        };
                    });

                    let stationsWithValidation = allData.map(
                        (station, indexStation) => {
                            return {
                                id: indexStation + 1,
                                country: station[4],
                                esi: station[44],
                                value: station.map((item, index) => {
                                    return {
                                        id: index,
                                        value: item
                                    };
                                })
                            };
                        }
                    );

                    stationsWithValidation.forEach(station => {
                        let twins = stationsWithValidation.filter(
                            st =>
                                st.esi &&
                                st.esi === station.esi &&
                                st.country === station.country
                        );
                        station.invalid = twins.length > 1;
                    });

                    let invalidRow = stationsWithValidation.find(
                        item => item.invalid
                    );

                    if (invalidRow) {
                        stationsWithValidation.sort((station1, station2) => {
                            return station1.invalid ? -1 : 1;
                        });
                        response.validate = false;
                        response.duplicate = true;
                    }

                    response.stations = stationsWithValidation;
                }
            }
        }
        res.send(response);
    }
);

router.get('/admin/check-status', async function(req, res) {
    let response = {
        status: true
    };
    if (req.session.validate && session[req.session.id].oracleUpdating) {
        response.oracleUpdating = true;
    } else {
        delete session[req.session.id];
        delete req.session.stations;
    }

    res.send(response);
});

router.post('/admin/update-salesforce', async function(req, res) {
    let response = {
        status: false
    };

    const batchId = batch.getBatchId();
    if (batchId) {
        response.batchId = batchId;
        response.status = true;
        res.send(response);
    } else {
        const versionsDB = VersionsDataBase();
        let versionCurrent = versionsDB.getCurrentVersion();
        const csvDataBase = CSVDataBase();
        let allData = await csvDataBase.getDataByVersion(versionCurrent.value);
        allData.shift();
        salesforceUpdateAccounts(allData, (error, body) => {
            if (error) {
                response.error = error;
            } else {
                const result = JSON.parse(body);
                if (result.status === 'success') {
                    batch.addBatchId(result.batchId);
                    response.status = true;
                    response.batchId = result.batchId;
                } else if (result.status === 'error') {
                    response.status = false;
                    response.message = result.message;
                }
            }
            res.send(response);
        });
    }
});

router.post('/admin/check-state-salesforce-updating', async function(req, res) {
    let response = {
        status: false
    };
    const batchId = req.body.batchId;

    request.post(
        {
            url: SALESFORCE_CHECK_STATE_URL,
            proxy: PROXY,
            headers: {
                secretKey: SECRET_KEY
            },
            json: {
                batchId: batchId
            }
        },
        (postError, resp, body) => {
            if (postError) {
                console.log(postError);
            } else {
                const result = JSON.parse(body);
                if (result.status === 'process') {
                    response.status = true;
                    response.batchId = result.batchId;
                    response.countErrors = result.countErrors;
                    response.countDone = result.countDone;
                    response.countTotal = result.countTotal;
                } else if (result.status === 'complete') {
                    batch.removeBatchId();
                    response.status = true;
                    response.isDone = true;
                    response.batchId = result.batchId;
                    response.countErrors = result.countErrors;
                    response.countDone = result.countDone;
                    response.countTotal = result.countTotal;
                } else {
                    response.message = result.message;
                    if (result.cleanBatch) {
                        batch.removeBatchId();
                    }
                }
            }
            res.send(response);
        }
    );
});

router.post('/admin/update-data-base', async function(req, res) {
    let response = {
        status: false
    };

    if (req.session.validate) {
        response.sessionValid = req.session.validate;

        let stationsPart = req.body.stations;
        if (!req.session.stations) {
            req.session.stations = stationsPart;
            response.uploaded = req.session.stations.length;
        } else if (req.body.isLast) {
            req.session.stations = req.session.stations.concat(stationsPart);
            response.uploaded = req.session.stations.length;
            const stationsTotalArray = req.session.stations;
            req.session.stations = null;
            const versionsDB = VersionsDataBase();
            const csvDataBase = CSVDataBase();
            const stations = stationsTotalArray.map(stationRow => {
                return stationRow.value.map(item => item.value);
            });
            let versionCurrent;
            versionCurrent = versionsDB.addVersion();
            response.status = await csvDataBase.createFile(
                versionCurrent,
                stations
            );
            response.status = true;
            session[req.session.id] = {
                oracleUpdating: true
            };

            LocatorOracleDB.update(stations, () => {
                session[req.session.id] = {
                    oracleUpdating: false
                };
            });
        } else {
            req.session.stations = req.session.stations.concat(stationsPart);
            response.uploaded = req.session.stations.length;
        }
    }

    res.send(response);
});

router.post('/admin/get-report', async function(req, res) {
    let response = {
        status: false
    };
    if (req.session.validate) {
        response.sessionValid = req.session.validate;
        const batchId = req.body.batchId;
        request.get(
            {
                url: `${SALESFORCE_URL}/${batchId}`,
                proxy: PROXY,
                headers: {
                    secretKey: SECRET_KEY
                }
            },
            (getError, resp, body) => {
                if (!getError) {
                    let data = JSON.parse(JSON.parse(body));
                    response.status = true;
                    let reportObject = data.report;
                    let reportFileUrl = data.url;
                    let report = {};
                    if (reportObject) {
                        reportData = reportObject.split('\n');
                        const reportHeader = [
                            {
                                id: 0,
                                value: reportData.shift()
                            }
                        ];
                        const reportRows = reportData.map((item, index) => {
                            return {
                                id: index + 1,
                                value: [
                                    {
                                        id: 0,
                                        value: item
                                    }
                                ]
                            };
                        });
                        report = { columns: reportHeader, errors: reportRows };
                    }

                    response.report = report;
                    response.url = reportFileUrl;
                }
                res.send(response);
            }
        );
    } else {
        res.send(response);
    }
});

function salesforceUpdateAccounts(allStations, callback) {
    let stations = allStations.map(station => {
        return {
            name: station[0],
            address: station[1],
            city: station[2],
            postCode: station[3],
            country: station[4],
            telephone: station[7],
            esi: station[44]
        };
    });
    request.post(
        {
            url: SALESFORCE_URL,
            proxy: PROXY,
            headers: {
                secretKey: SECRET_KEY
            },
            json: {
                stationsJson: JSON.stringify(stations)
            }
        },
        (postError, response, body) => {
            if (postError) {
                callback(postError);
            } else {
                callback(null, body);
            }
        }
    );
}

/**
 * @description login user
 */
router.post('/admin/preview-restore', async function(req, res) {
    let response = {
        status: false
    };

    if (req.session.validate) {
        response.sessionValid = req.session.validate;
        const versionId = parseInt(req.body.versionId, 10);
        if (versionId) {
            const csvDataBase = CSVDataBase();
            let allData = await csvDataBase.getDataByVersion(versionId);
            if (allData.length) {
                let header = allData.shift();
                response.columns = header.map((item, index) => {
                    return {
                        id: index,
                        value: item
                    };
                });
                response.stations = allData.map((station, indexStation) => {
                    return {
                        id: indexStation + 1,
                        value: station.map((item, index) => {
                            return {
                                id: index,
                                value: item
                            };
                        })
                    };
                });

                response.status = true;
            }
        }
    }
    res.send(response);
});

module.exports = router;
