/* eslint-disable no-undef */
const FS = require('fs');
const CSV = require('csv-reader');
const ICONV = require('iconv-lite');
const XLSX = require('xlsx');

const CSV_VALID = './backend/database/csv/valid.csv';
const CSV_BACKUPS = './backend/database/csv';

class CSVDataBase {
    getCSVPath(version) {
        return `${CSV_BACKUPS}/${version.value}.csv`;
    }

    async previewData(filePath) {
        const workBook = XLSX.readFile(filePath);
        const csvTempPath = `${filePath}.csv`;
        XLSX.writeFile(workBook, csvTempPath, {
            bookType: 'csv'
        });
        let rows = await this.getAllRowsWithHeader(csvTempPath);
        FS.unlinkSync(csvTempPath);
        FS.unlinkSync(filePath);
        return rows;
    }

    async getDataByVersion(versionId) {
        const csvTempPath = `${CSV_BACKUPS}/${versionId}.csv`;
        let rows = await this.getAllRowsWithHeader(csvTempPath);
        return rows;
    }

    async getAllRowsWithHeader(filePath) {
        let rows = [];
        const parser = CSV({
            skipHeader: false,
            parseNumbers: true,
            trim: true,
            skipEmptyLines: true
        });

        const readStream = FS.createReadStream(filePath)
            .pipe(ICONV.decodeStream('utf8'))
            .pipe(parser);

        for await (const row of readStream) {
            rows.push(row);
        }
        return rows;
    }

    async getHeader(filePath) {
        let header;
        const parser = CSV({
            skipHeader: false
        });

        const readStream = FS.createReadStream(filePath)
            .pipe(ICONV.decodeStream('utf8'))
            .pipe(parser);

        for await (const row of readStream) {
            header = row;
            break;
        }
        return header;
    }

    async createFile(version, stations) {
        let workBook = [];
        let header = await this.getValidHeader();
        workBook.push(header);
        workBook = workBook.concat(stations);
        let wbArrayOfString = workBook.map(item => {
            item[7] = `"${item[7]}"`;
            item[1] = item[1].toString().replace(/[\n\r]/g, ' ');
            return item.join(',');
        });
        let wbString = wbArrayOfString.join('\n');
        FS.writeFileSync(`${CSV_BACKUPS}/${version.value}.csv`, wbString);
        return true;
    }

    async getValidHeader() {
        const header = await this.getHeader(CSV_VALID);
        return header;
    }

    async isFileValid(header) {
        let result = true;
        const validHeader = await this.getHeader(CSV_VALID);
        if (validHeader.length === header.length) {
            validHeader.forEach((item, index) => {
                if (item !== header[index]) {
                    result = false;
                }
            });
        } else {
            result = false;
        }
        return result;
    }
}

let csvDataBase;

module.exports = () => {
    if (!csvDataBase) {
        csvDataBase = new CSVDataBase();
    }
    return csvDataBase;
};
