/* eslint-disable no-undef */
const moment = require('moment');
const fs = require('fs');
const DATA = './backend/database/versions.json';

class VersionsDataBase {
    getData() {
        let rawdata = fs.readFileSync(DATA);
        return JSON.parse(rawdata.toString());
    }

    getCurrentVersion() {
        let versions = this.getData();
        let currentVersion = versions.find(version => version.active);
        if (currentVersion) {
            currentVersion.title = this.getTitleVersion(currentVersion.value);
        }
        return currentVersion;
    }

    addVersion() {
        let versions = this.getData();
        let addId = Date.now();
        versions.forEach(element => {
            element.active = false;
        });
        versions.push({
            value: addId,
            active: true
        });
        fs.writeFileSync(DATA, JSON.stringify(versions));
        return {
            value: addId,
            label: this.getTitleVersion(addId)
        };
    }

    getListOfVersions() {
        let versions = this.getData();
        versions.sort((v1, v2) => v2.value - v1.value);
        return versions.map(version => {
            const value = version.value;
            return {
                value: value,
                label: this.getTitleVersion(value)
            };
        });
    }

    changeVersion(versionId) {
        let versions = this.getData();
        let activeId = null;
        versions.forEach(element => {
            element.active = false;
            const value = element.value;
            if (versionId === value) {
                element.active = true;
                activeId = value;
            }
        });
        fs.writeFileSync(DATA, JSON.stringify(versions));
        return {
            value: activeId,
            label: this.getTitleVersion(activeId)
        };
    }

    getTitleVersion(versionId) {
        return moment(versionId, 'x').format('DD.MM.YYYY | HH:mm:ss');
    }
}

let versionsDB;

module.exports = () => {
    if (!versionsDB) {
        versionsDB = new VersionsDataBase();
    }
    return versionsDB;
};
