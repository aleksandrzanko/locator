const batch = require('../batch');

describe('batch testing', () => {
    afterAll(() => {
        batch.removeBatchId();
    });

    it('test batch', () => {
        let addResult = batch.addBatchId('testid');
        expect(addResult).toBeTruthy();

        let getResult = batch.getBatchId();
        expect(getResult).toBe('testid');

        addResult = batch.addBatchId('testid2');
        expect(addResult).toBeFalsy();

        batch.removeBatchId();

        getResult = batch.getBatchId();
        expect(getResult).toBeNull();
    });
});
