const fs = require('fs');
const BATCH_PATH = './backend/database/batch';

const batch = {
    getBatchId: () => {
        let result = null;
        if (fs.existsSync(BATCH_PATH)) {
            let batchId = fs.readFileSync(BATCH_PATH);
            result = batchId.toString();
        }
        return result;
    },

    addBatchId: batchId => {
        let result = false;
        if (!fs.existsSync(BATCH_PATH)) {
            fs.writeFileSync(BATCH_PATH, batchId);
            result = true;
        }
        return result;
    },
    removeBatchId: () => {
        fs.unlinkSync(BATCH_PATH);
    }
};

module.exports = batch;
