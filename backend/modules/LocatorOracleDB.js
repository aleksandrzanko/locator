/* eslint-disable no-undef */
// Oracle

const oracledb = require('oracledb');
const fs = require('fs');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
const oracledbSettings = {
    user: 'POS_LOCATOR_EU',
    password: 'pos_locator_eu',
    connectString: 'srv-eurolocator-db-demo.transitcard.ru/WEBDB'
};

let optionsOracle = {
    autoCommit: true,
    bindDefs: [
        {
            type: oracledb.STRING,
            maxSize: 100
        },
        {
            type: oracledb.STRING,
            maxSize: 100
        },
        {
            type: oracledb.STRING,
            maxSize: 1024
        },
        {
            type: oracledb.STRING,
            maxSize: 255
        },
        {
            type: oracledb.STRING,
            maxSize: 100
        },
        {
            type: oracledb.STRING,
            maxSize: 100
        },
        {
            type: oracledb.NUMBER
        },
        {
            type: oracledb.NUMBER
        },
        {
            type: oracledb.STRING,
            maxSize: 100
        },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        {
            type: oracledb.STRING,
            maxSize: 100
        },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER },
        { type: oracledb.NUMBER }
    ]
};

const sqlStrDropTable = `BEGIN
                            EXECUTE IMMEDIATE 'DROP TABLE IMPORT_DATA_SHELL';
                            EXCEPTION
                                WHEN OTHERS THEN
                                IF SQLCODE != -942 THEN
                                    RAISE;
                                END IF;
                        END;`;

const sqlStrProcedureImportTable = `BEGIN
                                        shell_data_import.import_data_shell_table;
                                    END;`;

const sqlStrInsertValues = `INSERT INTO IMPORT_DATA_SHELL VALUES (
                            :1, :2, :3, :4, :5,
                            :6, :7, :8, :9, :10,
                            :11, :12, :13, :14, :15,
                            :16, :17, :18, :19, :20,
                            :21, :22, :23, :24, :25,
                            :26, :27, :28, :29, :30,
                            :31, :32, :33, :34, :35,
                            :36, :37, :38, :39, :40,
                            :41, :42, :43, :44, :45,
                            :46
                        )`;

const sqlStrCreateTable = `CREATE TABLE IMPORT_DATA_SHELL (
                            brand_name VARCHAR2(100),
                            pos_name VARCHAR2(200),
                            address VARCHAR2(400),
                            town VARCHAR2(100),
                            post_code VARCHAR2(15),
                            country VARCHAR2(255),
                            gps_latitude NUMBER(18,6),
                            gps_longitude NUMBER(18,6),
                            telephone VARCHAR2(200),
                            international_multi NUMBER(1),
                            international_single NUMBER(1),
                            station_id VARCHAR2(30),
                            hour_24 NUMBER(1),
                            shop NUMBER(1),
                            car_wash NUMBER(1),
                            toilet NUMBER(1),
                            shower NUMBER(1),
                            atm NUMBER(1),
                            restaurant NUMBER(1),
                            pay_at_pump NUMBER(1),
                            truck_friendly NUMBER(1),
                            truck_only NUMBER(1),
                            parking_lanes NUMBER(1),
                            motorway NUMBER(1),
                            guarded_parking NUMBER(1),
                            truck_adblue NUMBER(1),
                            partner_card NUMBER(1),
                            migrolino NUMBER(1),
                            spar_express NUMBER(1),
                            ad_blue_pump_truck NUMBER(1),
                            ad_blue_packed_roduct NUMBER(1),
                            pizza_hut_express NUMBER(1),
                            budgens NUMBER(1),
                            deli_cafe NUMBER(1),
                            amazon_locker NUMBER(1),
                            deli_by_shell NUMBER(1),
                            car_wash_mpay NUMBER(1),
                            pay_tm NUMBER(1),
                            ad_blue_pump_pass_vehicles NUMBER(1),
                            shell_v_power_unleaded NUMBER(1),
                            shell_fuel_save_unleaded NUMBER(1),
                            shell_v_power_diesel NUMBER(1),
                            shell_fuel_save_diesel NUMBER(1),
                            autogas_lpg NUMBER(1),
                            shell_hydrogen NUMBER(1),
                            shell_recharge NUMBER(1)
                        )`;

module.exports = {
    update: function(stations, callback) {
        const oldFormat = stations.map(station => {
            let lat = station[5] ? parseFloat(station[5]) : 0;
            let lon = station[6] ? parseFloat(station[6]) : 0;
            let townStr = station[2] + '';
            let town = townStr.length > 50 ? townStr.substring(0, 50) : townStr;
            return [
                station[9] + '',
                station[0] + '',
                station[1] + '',
                town,
                station[3] + '',
                station[4] + '',
                lat,
                lon,
                station[7] + '',
                1,
                0,
                station[44] + '',
                stBool(station[10]),
                stBool(station[11]),
                stBool(station[12]),
                stBool(station[13]),
                stBool(station[14]),
                stBool(station[15]),
                stBool(station[16]),
                stBool(station[17]),
                stBool(station[18]),
                stBool(station[19]),
                stBool(station[20]),
                stBool(station[21]),
                stBool(station[22]),
                stBool(station[23]),
                stBool(station[24]),
                stBool(station[25]),
                stBool(station[26]),
                stBool(station[27]),
                stBool(station[28]),
                stBool(station[29]),
                stBool(station[30]),
                stBool(station[31]),
                stBool(station[32]),
                stBool(station[33]),
                stBool(station[34]),
                stBool(station[35]),
                stBool(station[36]),
                stBool(station[37]),
                stBool(station[38]),
                stBool(station[39]),
                stBool(station[40]),
                stBool(station[41]),
                stBool(station[42]),
                stBool(station[43])
            ];
        });
        oracledb.getConnection(oracledbSettings, async function(
            err,
            connection
        ) {
            if (err) {
                fs.appendFileSync('./data.log', new Date()  + ' : ' + err + '\n\n');
                callback(err);
            } else {
                await connection
                    .execute(sqlStrDropTable)
                    .catch(err => {
                        fs.appendFileSync('./data.log', new Date()  + ' -- Sql Str Drop Table -- ' + err + '\n\n');
                        callback(errDelete);
                    });
                await connection
                    .execute(sqlStrCreateTable)
                    .catch(err => {
                        fs.appendFileSync('./data.log', new Date()  + ' -- Sql Str Create Table -- ' + err + '\n\n');
                        callback(err);
                    });
                await connection
                    .executeMany(sqlStrInsertValues, oldFormat, optionsOracle)
                    .catch(err => {
                        fs.appendFileSync('./data.log', new Date()  + ' -- Sql Str Insert Values -- ' + err + '\n\n');
                        callback(err);
                    });

                await connection
                    .execute(sqlStrProcedureImportTable)
                    .catch(err => {
                        fs.appendFileSync('./data.log', new Date()  + ' -- Sql Str Procedure Import Table -- ' + err + '\n\n');
                        callback(err);
                    });

                callback(null, oldFormat);
            }
        });
    }
};

function stBool(str) {
    let result = 0;
    if (str) {
        result = str.toLowerCase() === 'true' ? 1 : 0;
    }
    return result;
}
