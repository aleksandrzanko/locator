const ASSETS = require('../assets');

describe('assets testing', () => {
    it('contains "admin" in assets', () => {
        expect('admin' in ASSETS).toBeTruthy();
    });
});
