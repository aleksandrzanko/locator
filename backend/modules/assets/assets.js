const fs = require('fs'),
    path = require('path');

const assets = path.join(__dirname, './../../../public/assets.json');
const ASSETS = JSON.parse(fs.readFileSync(assets), 'utf8');

module.exports = ASSETS;
