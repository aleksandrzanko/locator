const { jestConfig } = require('@salesforce/sfdx-lwc-jest/config');
const ARGV = process.argv;

let collectCoverageFrom = [];
let moduleNameMapper = {};

if (ARGV.indexOf('frontend') > -1) {
    collectCoverageFrom.push('**/frontend/components/**/*.{js,jsx}');
    moduleNameMapper = {
        '^.+\\.(scss)$': 'identity-obj-proxy',
        '^.+\\.(svg|png)$': '<rootDir>/frontend/__mocks__/svgrMock.js',
        '^z(.+)': '<rootDir>/frontend/components/$1/$1'
    };
} else if (ARGV.indexOf('backend') > -1) {
    collectCoverageFrom.push('**/backend/modules/**/*.{js,jsx}');
    collectCoverageFrom.push('**/backend/routes/**/*.{js,jsx}');
}

module.exports = {
    ...jestConfig,
    moduleNameMapper: moduleNameMapper,
    collectCoverageFrom: collectCoverageFrom
};
